package org.robatipoor.polls.repository;

import java.util.Optional;

import org.robatipoor.polls.model.Role;
import org.robatipoor.polls.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * RoleRepository
 */
@Repository
public interface RoleRepository extends JpaRepository<Role ,Long> {
    Optional<Role> findByRoleName(RoleName roleName);
}