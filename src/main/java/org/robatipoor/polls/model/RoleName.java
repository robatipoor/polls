package org.robatipoor.polls.model;

/**
 * RoleName
 */
public enum RoleName {
    ROLE_ADMIN, ROLE_USER
}