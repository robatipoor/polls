package org.robatipoor.polls.model.audit;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * DateAudit
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createAt", "updateAt" }, allowGetters = true)
public abstract class DateAudit implements Serializable{

    @CreatedDate
    @Column(nullable = false,updatable = false)
    private Instant creatAt;
    @LastModifiedDate
    @Column(nullable = false)
    private Instant updateAt;

    public DateAudit() {
    }

    public DateAudit(Instant creatAt, Instant updateAt) {
        this.creatAt = creatAt;
        this.updateAt = updateAt;
    }

    public Instant getCreatAt() {
        return this.creatAt;
    }

    public void setCreatAt(Instant creatAt) {
        this.creatAt = creatAt;
    }

    public Instant getUpdateAt() {
        return this.updateAt;
    }

    public void setUpdateAt(Instant updateAt) {
        this.updateAt = updateAt;
    }

    public DateAudit creatAt(Instant creatAt) {
        this.creatAt = creatAt;
        return this;
    }

    public DateAudit updateAt(Instant updateAt) {
        this.updateAt = updateAt;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof DateAudit)) {
            return false;
        }
        DateAudit dateAudit = (DateAudit) o;
        return Objects.equals(creatAt, dateAudit.creatAt) && Objects.equals(updateAt, dateAudit.updateAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creatAt, updateAt);
    }

    @Override
    public String toString() {
        return "{" +
            " creatAt='" + getCreatAt() + "'" +
            ", updateAt='" + getUpdateAt() + "'" +
            "}";
    }

}